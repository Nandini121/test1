
public class SubstringForKids {
	
	

		public static void main(String[] args) {
			System.out.println("SUBSTRING FOR KIDS");
			
		}

		private int start_num;
		private int end_num;
		private String sentence;
		
		public String substringForKids(int start_num, int end_num, String sentence) {
			
			 this.setStart_num(start_num);
			 this.setEnd_num(end_num);
			 this.setSentence(sentence);
			return "";
		}

		public int getEnd_num() {
			return end_num;
		}

		public void setEnd_num(int end_num) {
			this.end_num = end_num;
		}

		public int getStart_num() {
			return start_num;
		}

		public void setStart_num(int start_num) {
			this.start_num = start_num;
		}

		public String getSentence() {
			return sentence;
		}

		public void setSentence(String sentence) {
			this.sentence = sentence;
		}
	}

